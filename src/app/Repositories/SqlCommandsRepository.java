package app.Repositories;

import java.util.HashMap;
import java.util.Map;

public class SqlCommandsRepository {

    public final static String getCreateDb(String dbname, String owner) {
       return "CREATE DATABASE " + dbname +
            "  WITH OWNER = " + owner +
            "       ENCODING = 'UTF8'\n" +
            "       TABLESPACE = pg_default\n" +
            "       LC_COLLATE = 'Russian_Russia.1251'\n" +
            "       LC_CTYPE = 'Russian_Russia.1251'\n" +
            "       CONNECTION LIMIT = -1\n" +
            "\n";
    }

    public final static String getDropDb (String dbname) { return "DROP DATABASE IF EXISTS " + dbname + " ;"; }

    public final static String getDropTable(String table)  { return "DROP TABLE IF EXISTS " + table + " CASCADE; "; }

    public final static String getClearTable(String table) { return "TRUNCATE " + table + " RESTART IDENTITY CASCADE; "; }


    public static final Map<String, String> TABLES;
    static {
        TABLES = new HashMap<String, String>();
        TABLES.put("relatives", "CREATE TABLE IF NOT EXISTS  relatives (\n" +
                "            parent_id INTEGER,\n" +
                "            child_id  INTEGER,\n" +
                "            PRIMARY KEY (parent_id, child_id),\n" +
                "            FOREIGN KEY (parent_id) REFERENCES pages(id)," +
                "            FOREIGN KEY (child_id) REFERENCES pages(id));");

        TABLES.put("tag_decoration", "CREATE TABLE IF NOT EXISTS  tag_decoration (" +
                "            id   SERIAL    NOT NULL,\n" +
                "            value TEXT,\n" +
                "            page_id INTEGER NOT NULL,\n" +
                "            PRIMARY KEY (id),\n" +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");
        TABLES.put("tag_u", "CREATE TABLE IF NOT EXISTS  tag_u (" +
                "            id   SERIAL    NOT NULL,\n" +
                "            value TEXT,\n" +
                "            page_id INTEGER NOT NULL,\n" +
                "            PRIMARY KEY (id),\n" +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");
        TABLES.put("tag_italic", "CREATE TABLE IF NOT EXISTS  tag_italic (" +
                "            id   SERIAL    NOT NULL,\n" +
                "            value TEXT,\n" +
                "            page_id INTEGER NOT NULL,\n" +
                "            PRIMARY KEY (id),\n" +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");
        TABLES.put("tag_bold", "CREATE TABLE IF NOT EXISTS  tag_bold (" +
                "            id    SERIAL    NOT NULL,\n" +
                "            value TEXT,\n" +
                "            page_id INTEGER NOT NULL,\n" +
                "            PRIMARY KEY (id),\n" +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");


        TABLES.put("tag_div", "CREATE TABLE IF NOT EXISTS  tag_div (" +
                "            id   SERIAL    NOT NULL,\n" +
                "            value TEXT,\n" +
                "            page_id INTEGER NOT NULL,\n" +
                "            PRIMARY KEY (id),\n" +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");
        TABLES.put("tag_span", "CREATE TABLE IF NOT EXISTS  tag_span (" +
                "            id   SERIAL    NOT NULL,\n" +
                "            value TEXT,\n" +
                "            page_id INTEGER NOT NULL,\n" +
                "            PRIMARY KEY (id),\n" +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");


        TABLES.put("tag_h1", "CREATE TABLE IF NOT EXISTS  tag_h1 (" +
                "            id  SERIAL  NOT NULL," +
                "            value TEXT NOT NULL," +
                "            page_id INTEGER NOT NULL," +
                "            PRIMARY KEY (id)," +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");
        TABLES.put("tag_h2", "CREATE TABLE IF NOT EXISTS  tag_h2 (" +
                "            id  SERIAL NOT NULL," +
                "            value TEXT," +
                "            page_id INTEGER NOT NULL," +
                "            PRIMARY KEY (id)," +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");
        TABLES.put("tag_h3", "CREATE TABLE IF NOT EXISTS  tag_h3 (" +
                "            id  SERIAL NOT NULL," +
                "            value TEXT," +
                "            page_id INTEGER NOT NULL," +
                "            PRIMARY KEY (id)," +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");

        TABLES.put("tag_a", "CREATE TABLE IF NOT EXISTS  tag_a (" +
                "            id  SERIAL     NOT NULL," +
                "            value TEXT," +
                "            href TEXT," +
                "            page_id INTEGER NOT NULL," +
                "            PRIMARY KEY (id)," +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");

        TABLES.put("tag_nav", "CREATE TABLE IF NOT EXISTS  tag_nav (" +
                "            id  SERIAL     NOT NULL," +
                "            value TEXT," +
                "            href TEXT," +
                "            page_id INTEGER NOT NULL," +
                "            PRIMARY KEY (id)," +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");

        TABLES.put("tag_img", "CREATE TABLE IF NOT EXISTS  tag_img (" +
                "            id  SERIAL NOT NULL," +
                "            src TEXT," +
                "            alt TEXT," +
                "            page_id INTEGER  NOT NULL," +
                "            PRIMARY KEY (id)," +
                "            FOREIGN KEY (page_id) REFERENCES pages(id));");

        TABLES.put("category_page", "CREATE TABLE IF NOT EXISTS  category_page (" +
                "            id          SERIAL NOT NULL," +
                "            category_id INTEGER," +
                "            page_id     INTEGER," +
                "            score       REAL," +
                "            PRIMARY KEY (id)," +
                "            FOREIGN KEY (category_id) REFERENCES categories(id), " +
                "            FOREIGN KEY (page_id) REFERENCES pages(id)); ");
        TABLES.put("categories", "CREATE TABLE IF NOT EXISTS  categories ( " +
                "            id    SERIAL NOT NULL, " +
                "            title TEXT, " +
                "            PRIMARY KEY (id));");

        TABLES.put("pages", "CREATE TABLE IF NOT EXISTS  pages (" +
                "            id     SERIAL NOT NULL, " +
                "            url    TEXT   NOT NULL, " +
                "            html_code  TEXT, " +
                "            html_text  TEXT, " +
                "            title  TEXT, " +
                "            meta_keywords  TEXT, " +
                "            meta_description  TEXT, " +
                "            p_text  TEXT, " +
                "            content  TEXT, " +
                "            header  TEXT, " +
                "            footer  TEXT, " +
                "            error INTEGER, " +
                "            domain_id INTEGER," +
                "            PRIMARY KEY(id)," +
                "            FOREIGN KEY (domain_id) REFERENCES domains(id));");

        TABLES.put("whois", "CREATE TABLE IF NOT EXISTS  whois ( " +
                "            id          SERIAL NOT NULL, " +
                "            value       TEXT, " +
                "            domain_id   INTEGER NOT NULL, " +
                "            PRIMARY KEY (id), " +
                "            FOREIGN KEY (domain_id) REFERENCES domains(id));");


        TABLES.put("domains", "CREATE TABLE IF NOT EXISTS  domains ( " +
                "            id   SERIAL NOT NULL PRIMARY KEY, " +
                "            url  TEXT NOT NULL);");
    };



}




