package app.Controllers;

import app.Repositories.SqlCommandsRepository;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.sql.*;
import java.util.Map;


public class MainController {

    static final String dbname = "webdb";
    String username, password, hostname;
    boolean isDbExists = false;

    @FXML
    private PasswordField Password;
    @FXML
    private TextField Host, Login;
    @FXML
    private Text Message;
    @FXML
    private Tab TabConnect, TabManager, TabInfo;
    @FXML
    private TabPane TabRoot;


    private Connection connection = null;

    @FXML
    public void initialize() {
        //throw new Exception("HELLO");
        this.Password.setText("123456");
        this.Login.setText("postgres");
        this.Host.setText("localhost:5432");
    }



    private String getHostString() {
        return "jdbc:postgresql://" + Host.getText() + "/";
    }

    // <Tab: Connect>
    private boolean isFormValid() {
        return !Host.getText().trim().isEmpty() && !Login.getText().trim().isEmpty();
    }

    @FXML
    private void connect(){
        TabManager.setDisable(true);

        if (!isFormValid()) {
            Message.setText("Wrong input! Please fill in all required fields!");
            return;
        }

        username = Login.getText();
        password = Password.getText();
        hostname = getHostString();

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            Message.setText("ERROR: no postgresql driver!");
            e.printStackTrace();
            return;
        }

        try {
            if (!connectToDb()) {
                connectToServer();
                ManagerMessage.setText(dbname + " does not exist!");
            }
        } catch (Exception ex) {
            Message.setText("Server address and/or Login and/or password are wrong! Correct information and try again.");
            return;
        }
        Message.setText("");
        TabConnect.setDisable(true);
        TabManager.setDisable(false);
        TabRoot.getSelectionModel().select(TabManager);
    }

    private boolean connectToServer() throws SQLException {
        connection = DriverManager.getConnection(hostname, username, password);
        TabInfo.setDisable(true);
        ManagerMessage.setText("Connect to Server");
        isDbExists = false;
        return isDbExists;
    }
    private boolean connectToDb() throws SQLException {
        isDbExists = false;
        try {
            connection = DriverManager.getConnection(hostname + dbname, username, password);
            ManagerMessage.setText(dbname + " is exist!");
            TabInfo.setDisable(false);
            isDbExists = true;
            return true;
        } catch (SQLException e) {
            ManagerMessage.setText("ERROR: Cannot connect to DB!");
            return false;
        }
    }
    // </Tab: Connect>



    // <Tab: Manager>
    @FXML
    private Text ManagerMessage;

    private void doSqlCommand(String query) throws SQLException {
        Statement statement;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(query);
            statement.close();
        } catch (SQLException e) {
            ManagerMessage.setText("ERROR: " + e.getMessage());
            throw new SQLException(e.getMessage() + " " + e.getNextException());
        }
    }

    @FXML
    private void createDb() throws SQLException  {
        doSqlCommand(SqlCommandsRepository.getCreateDb(dbname, username));
        try {
            connection.close();
            connectToDb();
            ManagerMessage.setText("Database was created!");
        } catch (SQLException e) {
            ManagerMessage.setText("ERROR: " + e.getMessage());
            throw new SQLException(e.getMessage() + " " + e.getNextException());
        }
    }

    @FXML
    private void createTables() throws SQLException {
        ManagerMessage.setText("");
        if (!isDbExists) {
            ManagerMessage.setText("ERROR: Sorry, it is impossible! Database does not exist!");
            return;
        }
        Statement statement;
        try {
            statement = connection.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE
            );

            connection.setAutoCommit(false);

            statement.addBatch(SqlCommandsRepository.TABLES.get("domains"));
            statement.addBatch(SqlCommandsRepository.TABLES.get("pages"));
            statement.addBatch(SqlCommandsRepository.TABLES.get("categories"));

            for(Map.Entry<String, String> table: SqlCommandsRepository.TABLES.entrySet()) {
                statement.addBatch(table.getValue());
            }

            statement.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ManagerMessage.setText("ERROR: " + e.getMessage());
            throw new SQLException(e.getMessage() + " " + e.getNextException());
        }
        ManagerMessage.setText("All tables are ready!");
    }

    @FXML
    private void clearTables() throws SQLException {
        String query = "";
        for(Map.Entry<String, String> table: SqlCommandsRepository.TABLES.entrySet()) {
            query += SqlCommandsRepository.getClearTable(table.getKey());
        }
        doSqlCommand(query);
        ManagerMessage.setText("All tables has been cleaned...");
    }

    @FXML
    private void deleteDb() throws SQLException {
        connection.close();
        connectToServer();
        doSqlCommand(SqlCommandsRepository.getDropDb(dbname));
        ManagerMessage.setText("Database has been deleted...");
    }

    @FXML
    private void deleteTables() throws SQLException {
        String query = "";
        for(Map.Entry<String, String> table: SqlCommandsRepository.TABLES.entrySet()) {
            query += SqlCommandsRepository.getDropTable(table.getKey());
        }
        doSqlCommand(query);
        ManagerMessage.setText("Tables has been deleted...");
    }

    // </Tab: Manager>


    // <Tab: TabInfo>
    @FXML
    private void showTables() {

    }
    // </Tab: TabInfo>

}
